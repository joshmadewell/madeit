import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import utils from '../shared/utils';

import AccountsUI from './AccountsUI.jsx';

export default class Navigation extends TrackerReact(Component) {
	render() {
		return <nav className="navbar navbar-light bg-faded">
			<a className="navbar-brand" href="/all">Madeit</a>
			<ul className="nav navbar-nav">
				<li className="nav-item active">
					<a className="nav-link" href="/all">All Posts <span className="sr-only">(current)</span></a>
				</li>
				{ utils.isRole('admin') ?
					<li className="nav-item">
						<a className="nav-link" href="/users">Users</a>
					</li> :
					''
				}
				<li className="nav-item">
					<AccountsUI />
				</li>
			</ul>
		</nav>
	}
}