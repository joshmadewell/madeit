import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class PostForm extends TrackerReact(Component) {
	constructor() {
		super();

		this.state = {
			titleRequiredError: false,
			urlRequiredError: false,
			post: {},
			subscription: {
				posts: Meteor.subscribe('allPosts')
			}
		}
	}

	componentWillUnmount() {
		this.state.subscription.posts.stop();
	}

	submitPost(event) {
		event.preventDefault();

		var title = this.refs.postTitle.value.trim();
		var url = this.refs.postUrl.value.trim();

		this.setState({
			titleRequiredError: title.length === 0,
			urlRequiredError: url.length === 0
		}, function () {
			if (this.state.titleRequiredError || this.state.urlRequiredError) {
				return;
			}

			let postData = {
				title: title,
				url: url,
				content: this.refs.postContent.value.trim()
			};

			if (this.props.id) {
				Meteor.call('editPost', this.props.id, postData, () => {
					FlowRouter.go('allPosts');
				});
			} else {
				Meteor.call('addPost', postData, () => {
					FlowRouter.go('allPosts');
				});
			}
		});
	}

	getPost() {
		return this.props.id ? Posts.findOne(this.props.id) : {
			title: '',
			url: '',
			content: ''
		};
	}

	cancel(event) {
		event.preventDefault();
		FlowRouter.go('allPosts');
	}

	render() {
		let post = this.getPost();

		if (typeof post === 'undefined') {
			return <div>Loading...</div>
		}

		return (
			<form className="new-post">
				<div className={ this.state.titleRequiredError ? "form-group has-error" : "form-group" }>
					<label htmlFor="post-title">Title</label>
					<input id="post-title" className="form-control" ref="postTitle" defaultValue={ post.title } placeholder="Title" />
					{ this.state.titleRequiredError ? <span className="error-message">Required</span> : '' }
				</div>

				<div className={ this.state.urlRequiredError ? "form-group has-error" : "form-group" }>
					<label htmlFor="post-url">Url</label>
					<input id="post-url" className="form-control" ref="postUrl" defaultValue={ post.url } placeholder="Url" />
					{ this.state.urlRequiredError ? <span className="error-message">Required</span> : '' }
				</div>

				<div className="form-group">
					<label htmlFor="post-content">Content</label>
					<textarea id="post-content"
						className="form-control"
						rows="5"
						ref="postContent"
						placeholder="Post Content"
						defaultValue={ post.content }>
					</textarea>
				</div>

				<div className="form-actions">
					<button className="btn btn-primary action"
						type="submit"
						onClick={ this.submitPost.bind(this) }>
						Submit
					</button>

					<button className="btn btn-danger action"
						onClick={ this.cancel.bind(this) }>
						Cancel
					</button>
				</div>
			</form>
		);
	}
}