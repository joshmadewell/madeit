import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

import CommentsListWrapper from '../commentsList/CommentsListWrapper';

import utils from '../../shared/utils';

export default class PostView extends TrackerReact(Component) {
	constructor() {
		super();

		this.state = {
			subscription: {
				posts: Meteor.subscribe('allPosts'),
				comments: Meteor.subscribe('allComments')
			}
		}
	}

	componentWillUnmount() {
		this.state.subscription.posts.stop();
		this.state.subscription.comments.stop();
	}

	getPost() {
		return Posts.findOne(this.props.id);
	}

	render() {
		let post = this.getPost();

		return !post ? <div>Loading...</div> : (
			<div className="post-view">
				<div className="panel panel-default">
					<div className="panel-heading">{ post.title }<br /><a href={ utils.ensureUrl(post.url) } target="_blank">{ utils.ensureUrl(post.url) }</a></div>
					<div className="panel-body">
						{ post.content }
					</div>
				</div>

				<CommentsListWrapper postId={ post._id }/>
			</div>
		)
	}
}