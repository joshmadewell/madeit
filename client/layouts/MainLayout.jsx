import React from 'react';
import Navigation from '../Navigation.jsx';

export const MainLayout = ({ content }) => (
	<div className="main-layout">
		<header>
			<Navigation />
		</header>

		<main>
			{ content }
		</main>
	</div>
)