import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import utils from '../../shared/utils';

export default class PostSingle extends TrackerReact(Component) {
	upvotePost(event) {
		event.stopPropagation();
		Meteor.call('votePost', this.props.post, true);
	}

	downvotePost(event) {
		event.stopPropagation();
		Meteor.call('votePost', this.props.post, false);
	}

	deletePost(event) {
		event.stopPropagation();
		Meteor.call('deletePost', this.props.post);
	}

	toggleComments(event) {
		event.stopPropagation();
	}

	editPost(event) {
		event.stopPropagation();
		FlowRouter.go('editPost', {
			postId: this.props.post._id
		});
	}

	viewPost() {
		FlowRouter.go('viewPost', {
			postId: this.props.post._id
		});
	}

	canModify() {
		return utils.isRole('admin') || utils.isRole('moderator') || utils.userIsOwnerOf(this.props.post);
	}

	render() {
		return (
			<li className="list-group-item madeit-post-row" onClick={ this.viewPost.bind(this) }>
				<div className="vote-section">
					<a className="btn"
						href="#"
						onClick={ this.upvotePost.bind(this) } >
						<i className="glyphicon glyphicon-chevron-up"></i>
					</a>
					<div className="vote-count">
						{ this.props.post.upvotes - this.props.post.downvotes }
					</div>
					<a className="btn"
						href="#"
						onClick={ this.downvotePost.bind(this) } >
						<i className="glyphicon glyphicon-chevron-down"></i>
					</a>
				</div>

				<label className="post-title">{ this.props.post.title }</label>

				<div className="post-actions">
					<a href="#" onClick={ this.toggleComments.bind(this) }>Comments ({ this.props.post.comments })&nbsp;</a>
					{ this.canModify() ? <a href="#" onClick={ this.editPost.bind(this) }>Edit&nbsp;</a> : '' }
					{ this.canModify() ? <a href="#" onClick={ this.deletePost.bind(this) }>Delete&nbsp;</a> : '' }
				</div>
			</li>
		)
	}
}