import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

import PostsListSingle from './PostsListSingle.jsx';

Posts = new Mongo.Collection('posts');

export default class PostsWrapper extends TrackerReact(React.Component) {
	constructor() {
		super();

		this.state = {
			subscription: {
				posts: Meteor.subscribe('allPosts'),
				comments: Meteor.subscribe('allComments')
			}
		}
	}

	componentWillUnmount() {
		this.state.subscription.posts.stop();
	}

	getPosts() {
		return Posts.find().fetch().map((post) => {
			post.comments = Comments.find({ postId: post._id }).count();
			return post;
		}).sort((a, b) => {
			var propA = a.upvotes - a.downvotes;
			var propB = b.upvotes - b.downvotes;
			return propB - propA;
		});
	}

	createPost() {
		FlowRouter.go('newPost');
	}

	render() {
		return (
			<div>
				<button onClick={ this.createPost.bind(this) }>Create Post</button>
				<ul className="list-group madeit-posts-list">
					{ this.getPosts().map((post) => {
						return <PostsListSingle key={ post._id } post={ post }/>
					})}
				</ul>
			</div>
		)
	}
}