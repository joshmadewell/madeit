import React from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

import CommentsListSingle from './CommentsListSingle.jsx';
import CommentForm from './CommentForm.jsx';

Comments = new Mongo.Collection('comments');

export default class PostsWrapper extends TrackerReact(React.Component) {
	constructor() {
		super();

		this.state = {
			showCommentForm: false,
			subscription: {
				comments: Meteor.subscribe('allComments')
			}
		}
	}

	componentWillUnmount() {
		this.state.subscription.comments.stop();
	}

	toggleCommentForm() {
		this.setState({
			showCommentForm: !this.state.showCommentForm
		});
	}

	getComments() {
		let comments = Comments.find({ postId: this.props.postId }).fetch().sort((a, b) => {
			var propA = a.upvotes - a.downvotes;
			var propB = b.upvotes - b.downvotes;
			return propB - propA;
		});

		return comments;
	}

	render() {
		return (
			<div>
				{ Meteor.userId() ?
					<button className="btn btn-primary btn-xs pull-right"
						onClick={ this.toggleCommentForm.bind(this) }>
						Comment
					</button> :
					''
				}

				{ this.state.showCommentForm ?
					<CommentForm postId={ this.props.postId }
						onCancel={ this.toggleCommentForm.bind(this) }
						onSubmit={ this.toggleCommentForm.bind(this) } />
					: '' }

				<ul className="list-group madeit-comments-list">
					{ this.getComments().map((comment) => {
						return <CommentsListSingle
									key={ comment._id }
									comment={ comment } />
					})}
				</ul>
			</div>
		)
	}
}