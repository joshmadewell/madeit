import React, { Component } from 'react';

export default class CommentForm extends Component {
	constructor() {
		super();

		this.state = {
			commentRequiredError: false,
		}
	}

	submitComment(event) {
		event.preventDefault();

		let comment = this.refs.commentContent.value.trim();

		this.setState({
			commentRequiredError: comment.length === 0,
		}, () => {
			if (this.state.commentRequiredError) {
				return;
			}

			let commentData = {
				content: comment
			};

			Meteor.call('addComment', this.props.postId, commentData);

			this.props.onSubmit();
		});
	}

	cancel(event) {
		event.preventDefault();
		this.props.onCancel();
	}

	render() {
		return (
			<form className="new-post">
				<div className="form-group">
					<label htmlFor="post-content">Comment</label>
					<textarea id="post-content"
						className="form-control"
						rows="5"
						ref="commentContent"
						placeholder="Comment">
					</textarea>
				</div>

				<div className="form-actions">
					<button className="btn btn-primary btn-xs action"
						type="submit"
						onClick={ this.submitComment.bind(this) }>
						Submit
					</button>

					<button className="btn btn-danger btn-xs action"
						onClick={ this.cancel.bind(this) }>
						Cancel
					</button>
				</div>
			</form>
		);
	}
}