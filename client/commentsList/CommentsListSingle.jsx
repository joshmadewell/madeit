import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import utils from '../../shared/utils';

export default class PostSingle extends TrackerReact(Component) {
	constructor() {
		super();

		this.state = {
			editingComment: false
		}
	}

	upvoteComment(event) {
		event.stopPropagation();
		Meteor.call('voteComment', this.props.comment, true);
	}

	downvoteComment(event) {
		event.stopPropagation();
		Meteor.call('voteComment', this.props.comment, false);
	}

	deleteComment(event) {
		event.stopPropagation();
		Meteor.call('deleteComment', this.props.comment);
	}

	editComment(event) {
		event.stopPropagation();
		this.setState({
			editingComment: true
		})
	}

	saveComment(event) {
		event.stopPropagation();

		let comment = this.refs.commentContent.value.trim();

		if (comment.length === 0) {
			return;
		}

		Meteor.call('editComment', this.props.comment._id, {
			content: comment
		})

		this.setState({
			editingComment: false
		})
	}

	cancelEdit(event) {
		event.stopPropagation();
		this.setState({
			editingComment: false
		})
	}

	canModify() {
		return utils.isRole('admin') || utils.isRole('moderator') || utils.userIsOwnerOf(this.props.comment);
	}

	render() {
		return (
			<li className="list-group-item comment-item">
				{ this.state.editingComment ?
					<textarea className="comment-edit" rows="3" ref="commentContent" defaultValue={ this.props.comment.content }>
					</textarea>

					:

					this.props.comment.content
				}

				{ this.state.editingComment ?
					<div className="comment-actions">
						{ this.canModify() ? <a href="#" onClick={ this.saveComment.bind(this) }>Save&nbsp;</a> : '' }
						{ this.canModify() ? <a href="#" onClick={ this.cancelEdit.bind(this) }>Cancel&nbsp;</a> : '' }
					</div>

					:

					<div className="comment-actions">
						{ this.canModify() ? <a href="#" onClick={ this.editComment.bind(this) }>Edit&nbsp;</a> : '' }
						{ this.canModify() ? <a href="#" onClick={ this.deleteComment.bind(this) }>Delete&nbsp;</a> : '' }
					</div>
				}
			</li>
		)
	}
}