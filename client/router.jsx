import React from 'react';
import { mount } from 'react-mounter';

import { MainLayout } from './layouts/MainLayout.jsx';

import PostsListWrapper from './postsList/PostsListWrapper.jsx';
import PostForm from './post/PostForm.jsx';
import PostView from './post/PostView.jsx';

import UsersListWrapper from './usersList/UsersListWrapper.jsx';

FlowRouter.route('/', {
	action() {
		FlowRouter.go('allPosts')
	}
})

FlowRouter.route('/all', {
	name: 'allPosts',
	action() {
		mount(MainLayout, {
			content: (<PostsListWrapper />)
		});
	}
})

FlowRouter.route('/post/new', {
	name: 'newPost',
	action() {
		mount(MainLayout, {
			content: (<PostForm />)
		});
	}
})

FlowRouter.route('/post/:postId', {
	name: 'viewPost',
	action(params) {
		mount(MainLayout, {
			content: (<PostView id={ params.postId } />)
		});
	}
})

FlowRouter.route('/post/:postId/edit', {
	name: 'editPost',
	action(params) {
		mount(MainLayout, {
			content: (<PostForm id={ params.postId }/>)
		});
	}
})

FlowRouter.route('/users', {
	name: 'users',
	action() {
		mount(MainLayout, {
			content: (<UsersListWrapper />)
		});
	}
})