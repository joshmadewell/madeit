import React, { Component } from 'react';
import utils from '../../shared/utils';

export default class UserSingle extends Component {
	setRole(role) {
		Meteor.call('updateUserRole', this.props.user._id, role);
	}

	render() {
		return (
			<tr>
				<td>{ this.props.user.emails[0].address }</td>
				<td>{ this.props.user.emails[0].address }</td>
				<td>
					<div className="dropdown">
						<button className="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							{ this.props.user.profile.roles[0] }
							<span className="caret"></span>
						</button>
						<ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
							<li><a href="#" onClick={ this.setRole.bind(this, 'admin') }>Admin</a></li>
							<li><a href="#" onClick={ this.setRole.bind(this, 'moderator') }>Moderator</a></li>
							<li><a href="#" onClick={ this.setRole.bind(this, 'poster') }>Poster</a></li>
						</ul>
					</div>
				</td>
			</tr>
		)
	}
}