import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import utils from '../../shared/utils';

import UsersListSingle from './UsersListSingle.jsx';

export default class UsersWrapper extends TrackerReact(Component) {
	constructor() {
		super();

		this.state = {
			isAdmin: utils.isRole('admin'),
			subscription: {
				users: Meteor.subscribe('userList')
			}
		}
	}

	componentWillUnmount() {
		this.state.subscription.users.stop();
	}

	getUsers() {
		return Meteor.users.find().fetch();
	}

	render() {
		return utils.isRole('admin') ? (
			<div className="table-responsive">
				<table className="table">
					<thead>
						<tr>
							<td>Email</td>
							<td>Username</td>
							<td>Role</td>
						</tr>
					</thead>
					<tbody>
						{ this.getUsers().map((user) => {
							return <UsersListSingle key={ user._id } user={ user }/>
						})}
					</tbody>
				</table>
			</div>
		) : <div>Admins Only</div>;
	}
}