#### Learn Meteor (Time boxing at 4hours)

#### As a user, I would like an Admin account upon creation so that I can always login to the app.
	- Use Meteor startup to create user if no users exist - 30min

#### As a user, I would like to be able to make an account that I can log into later.
	- Investigate accounts packages - 1hr
	- Implement accounts - 1hr
	- Create roles - 30min

#### As a user, I would like to be able to create a post, so that users can see how cool i am.
	- Create NewPost form template - 1hr
	- Create Meteor method to create post - 30m

#### As a user, I would like to be able to see all posts, and be able to edit those which I are my own.
	- Create posts lists page - 1.5hrs
	- Create single post list template - 1.5hrs
	- Create actions to edit post - 10m
	- Implement editing in NewPost template and refactor as PostForm.jsx - 1hr

#### As a user, I would like to be able to comment, so I can tell people how I feel about their post
	- Create comment form. 15m
	- Create inline edit for comments - 15m
	- Create actions for comment - 15m

#### As an admin, I would like to be able modify all things, so that I can be an admin.
	- Implement `canModify` on posts and comments to allow admins or owners to delete. - 5min
	- Implement user list page - 1hr
	- Create UI for editing user roles - 15m
	- Ensure "posters" cannot see users page

#### As a moderator, I would like to be able delete posts and comments, so that I can moderate.
	- Refactor `canModify` code to include moderators. 15m
	- Ensure users still only show to admins. 5min

