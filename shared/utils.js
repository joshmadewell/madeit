const utils = {
	isRole(role) {
		let user = Meteor.user() || {};

		if (user.profile && user.profile.roles &&
			user.profile.roles.indexOf(role) !== -1) {
			return true;
		}

		return false;
	},

	userIsOwnerOf(ownerOf) {
		let user = Meteor.user() || {};

		if (ownerOf && ownerOf.ownerId === user._id) {
			return true;
		}

		return false;
	},

	ensureUrl(url) {
		if (!/^https?:\/\//i.test(url)) {
			return 'http://' + url;
		}

		return url;
	}
}

export default utils;