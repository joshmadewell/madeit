Meteor.startup(() => {
	if (Meteor.users.find().count() === 0) {
		Accounts.createUser({
			email: 'admin@madeit.com',
			password: 'admin',
			profile: {
				roles: ['admin']
			}
		})
	}

	Accounts.onCreateUser((options, user) => {
		user.profile = {
			roles: ['poster']
		}

		return user;
	})
});