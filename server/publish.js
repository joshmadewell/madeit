Posts = new Mongo.Collection('posts');
Comments = new Mongo.Collection('comments');

Meteor.publish('allPosts', function () {
	return Posts.find();
});

Meteor.publish('allComments', function () {
	return Comments.find();
});

Meteor.publish('userPosts', function () {
	return Posts.find({ user: this.userId });
});

Meteor.publish('userList', function () {
	return Meteor.users.find({});
});