Meteor.methods({
	addPost(post) {
		if (!Meteor.userId()) {
			throw new Meteor.Error('not-logged-in');
		}

		Posts.insert({
			title: post.title,
			url: post.url,
			content: post.content,
			upvotes: 1,
			downvotes: 0,
			comments: [],
			createdAt: new Date(),
			ownerId: Meteor.userId()
		});
	},
	editPost(id, postData) {
		Posts.update(id, {
			$set: postData
		});
	},
	togglePost(post) {
		if (Meteor.userId() !== post.user) {
			throw new Meteor.Error('not-authorized');
		}

		Posts.update(post._id, {
			$set: { complete: !post.status }
		});
	},
	deletePost(post) {
		if (Meteor.call('isRole', 'admin') || Meteor.call('isRole', 'moderator') || Meteor.call('userIsOwnerOf', post)) {
			Posts.remove(post._id);
		} else {
			throw new Meteor.Error('not-authorized');
		}
	},
	votePost(post, isUpvote) {
		if (!Meteor.userId()) {
			throw new Meteor.Error('Must be logged in to vote.');
		}

		if (isUpvote) {
			Posts.update(post._id, {
				$inc: { upvotes: 1 }
			});
		} else {
			Posts.update(post._id, {
				$inc: { downvotes: 1 }
			});
		}
	}
})