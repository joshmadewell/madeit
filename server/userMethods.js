Meteor.methods({
	updateUserRole(userId, role) {
		Meteor.users.update(userId, {
			$set: {
				profile: {
					roles: [ role ]
				}
			}
		})
	}
})