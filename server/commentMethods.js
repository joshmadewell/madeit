Meteor.methods({
	addComment(postId, comment) {
		if (!Meteor.userId()) {
			throw new Meteor.Error('not-logged-in');
		}

		Comments.insert({
			content: comment.content,
			postId: postId,
			upvotes: 1,
			downvotes: 0,
			createdAt: new Date(),
			ownerId: Meteor.userId()
		});
	},
	editComment(id, commentData) {
		Comments.update(id, {
			$set: commentData
		});
	},
	deleteComment(comment) {
		if (Meteor.call('isRole', 'admin') || Meteor.call('isRole', 'moderator') || Meteor.call('userIsOwnerOf', comment)) {
			Comments.remove(comment._id);
		} else {
			throw new Meteor.Error('not-authorized');
		}
	},
	voteComment(comment, isUpvote) {
		if (!Meteor.userId()) {
			throw new Meteor.Error('not-logged-in');
		}

		if (isUpvote) {
			Comments.update(comment._id, {
				$inc: { upvotes: 1 }
			});
		} else {
			Comments.update(comment._id, {
				$inc: { downvotes: 1 }
			});
		}
	}
})